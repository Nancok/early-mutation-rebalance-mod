# CDDA Early Mutation Rebalance
Mod for making mutations more accessible early game, without removing their late-game content.  

Made for version: e46ffd3   

07-03-2024 -2533
  
# Changes.txt  

You may delete any files of the mod as to disable changes:  
itemgroups.json > Item spawns  
armor_integrated.json > Mutation-provided items (like fur)  
mutations.json > Mutation rebalance  
recipes_chem_mutagen.json > Changes to mutagen crafting  
recipe_tools.json > Changes to chemistry tool crafting  
mutation_groups.json > Changes to threshold primer requirements  
enchantments.json > Required for some mutation's rebalancing, no effect otherwise.  

---Mutations---

Encumbrance values are based off similar gear with an extra reduction to match the fact that this is actual anatomy and not an added accessory.
Encumbrance is applied based on the penalties it applies, if a mutation is bulky but is exceptionally easy to handle, then encumbrance is lowered to match.

--Categories--
- all: minimum primer for breaking threshold 2200 -> 1200

This lowers the required amount of primer doses to reach the threshold from 10 to 6 and allows breaking it with just 3 doses of the advanced mutagen instead of 5

This treats thesholds as less of a long term goal and more of just a doorstop to avoid accidental transition since the transition is more of a downside than anything. Thresholds like bear's would otherwise require you to hunt over 10 bears JUST for the threshold. Triffids aren't that much common either.

--Arms and hands--  
Specific crafting skill bonuses have been removed, since these are just too general and will apply to things it probably shouldn't, only general encumbrance penalties remain.
Encumbrance values do not consider that the mutated body parts are fairly human-like and not a 1-1 to animal body parts. And also forgets that these aren't comparable to wearable gear since they are part of the character's anatomy, and therefore are more natural to use.

-Paws-  
// Human sized, just less flexible than a hand.
- Little Paws: encumbrance 5 -> 4 

// Hand like, but a bit too large for comfort.
- Paws: encumbrance 10 -> 6

// Equivalent to a loose leather glove, way too bulky for fine manipulation.
- Broad Paws: encumbrance 20 -> 10

For paws the description states "quasi-paws" or "look like paws". But that doesn't mean your character gets literal paws. I think it is fair to assess this as deformed to be more paw-like but still working as an actual hand with more limited finger and wrist dexterity. 

-Tentacles-
- Tentacle Arms/4 Tentacles/8 Tentacles: encumbrance 20 -> 8
- 8 Tentacles: restricts armor in arms as well as hands
- Tentacle Rakes: encumbrance 20 -> 9

In case no one has ever seen squid's ability to handle objects, it is pretty damn good. And yes, i am assuming some kind of suckers on the tentacles.  
Tentacles are not as fit for many things that a hand would, but they are still more dextrous than a human hand. They keep some encumbrance to reflect their inadequacy on every-day tasks.
Also, you're not fitting 4 arms into a sleeve anymore than you're fitting a tentacle in a glove.

-Insectoid-
- Arachnid Limbs: encumbrance 40 -> 5, removed dexterity penalty
- Insect Arms: encumbrance 30 -> 4, removed dexterity penalty

Primitive insectoid limbs have insane penalties for no reason. Even if they where flailing around 24/7, your character should have little difficulty restraining them. If anything it should be torso encumbrance but i'll spare that train of tought.

-Vines-
- Vines: encumbrance moved to arms, 10 -> 6
- Vine Limbs: encumbrance moved to arms, 10 -> 4

It is stated that these come from the shoulders, so there's no reason to affect the torso. At the same time, they are just hanging, so they are merely extra bulk to balance around.

-Leaves-
- Verdant Leaves/Autumn Foliage: encumbrance while covered 10 -> 8

There's no need to double encumbrance just for being a higher tier.

-Webbed-
- Webbed Hands: encumbrance while covered 50 -> 30, encumbrance 0 -> 7, removed dexterity penalty

I am not sure why this is the only hand mutation that opts for a dexterity penalty.
And yes, i get it, you shouldn't be wearing gloves, but that is insane.

--Legs and feet--  

-Gastropod-
- Gastropod Foot: noise modifier 0.25 -> 0.2, move cost multiplier 0.25 -> 0.33, encumbrance 10 -> 7

Blob knows this thing needs help. While the speed loss is warranted, the increased reliability should not incur such encumbrance. I'd leave this at 0 encumbrance but i feel there's something i am not getting fully.
The speed was increased regardless to not be as painful and since there's no steps being made, noise is even lower than the original value.

-Roots-
- Toe Roots/Roots/Rooter/Cloromorphosis: encumbrance 10 -> 7

These are described as toe roots, and the fact that encumbrance doesn't change means they don't change their bulk either. A few "roots" can't be THAT annoying. You already loose movement speed.


--Wings--  
//These are a bit large, but they are also the lightest kind of wings and are purely for flair. They should not be more cumbersome than a backpack.
- Butterfly Wings: encumbrance 10 -> 6, social modifiers reduced to +10 persuasion and -5 intimidation, dodge penalty -4 -> 0.

// While the fingers are webbed, they are still long fingers, squeezing one onto a trigger is probably awkward, but that's about it.
- Bat Wings: encumbrance arms 20 -> 12, hands 40 -> 14

While wings are some of the most unique kind of anatomy, they don't need these extreme levels of encumbrance. Being large doesn't mean that it is hard to move them around, and some of these values seem to hint at wings being a possible target, while-in game they cannot be targeted. Until that is a possibility i've removed penalties related to dodging.
In fact, wings could help with balance since it is already implied that they "catch air" and help with movement.

--Face--  
//"Slightly" hard to breath can't be that much worse than wearing a balaclava
- Slit Nostrils: encumbrance 10 -> 6


--Voice tones--  
Voice mutations had a lying modifier with no explanation and had some extreme values. Screeching and Croaking now get a small 10-15 bonus to lying due to how much they distort your voice , while the rest have no effect on this.
Voices that are good at intimidation now get higher intimidation boosts. If it's actually scary enough to make persuasion tank, then the intimidation should be similar in power. The intimidation bonuses are now around 70% of the persuasion loss instead of 50%.


--Body--

-Size-
// This is supposed to affect your whole body, there's no reason to give encumbrance to torso and arms only.
- Inconviniently Large: encumbrance removed, -1 dexterity.

-Ponderous-
- Ponderous: speed cost increase 10%->5%
- Very Ponderous: speed cost increase 20%->15%
- Extremely Ponderous: speed cost increase 30%->25%

Movement speed is the most important stat in the game, every % counts, and this also makes a lot of stuff more painful to deal with overall. Just a bit of a buff.

--Diet--

-Bloodfeeder-
- Hemovore: iron vitamin decay 1000 -> 600
- Bloodfeeder: iron vitamin decay 800 -> 750

Hemovore is the precursor to bloodfeeder, why does it have a higher decay rate? 

--Head--

-Bioluminescence-
- Reflex Photophore: encumbrance 5 -> 3
- Weak Photophore: encumbrance 5 -> 3, duration 225 hours > 1 hour, calories cost 9 -> 4
- Weak Photophore: encumbrance 5 -> 3, duration 112 hours and 30 minutes > 1 hour, calories cost 9 -> 4
- Bioluminescent Flare: encumbrance 5 -> 0, calory cost 400 -> 200

Activates for 225 hours so you just casually become incapable of hiding in the dark for like a week.
Atomic reading lights are not that rare to risk using this.
The flare did NOT replace the photophore, so it was adding 5 encumbrance for no reason.

--Misc--  
//This tires you already and requires prep time, no need to cost 400 calories when other leaps cost nothing.
  - Pounce: calory cost 400 -> 0
  
  
  
  
---Integrated Armor---  
Following the logic of mutations, mutated anatomy is part of the creature's natural body.
Furs other than bulkier ones like bear's has been reduced to 0 or 1. More bulkier types of skin like bark retain a very low value.

Armor mutations had considerable reductions but remain at problematic values worthy of their bulk.
Molted armors are less stiff and therefore less encumbering, allowing for easier escapes while your armor solidifies.
Additionally, since armors have gaps in joints, their encumbrance has been reduced and transferred to their adjacent limb. This should have no noticeable effect.


---Recipes---  
Recipes are kinda off. Mutagen has a massive bonus to speed when you cook over 20 in one go, themed mutagens take random themed ingredients like honey or anti-rads pills and the way of getting the necessary lab tools is very linear and gate-kept to late-game.
These changes are still lore-compliant to how mutagen works. But simply invents a different approach to producing it.
Remember that this is not IRL mutagen, this is basically a magical compound that coerces [REDACTED] into changing your body.

--Mutagen--

<"Low tech">
- mutagen/animal mutagens/purifier: batch saving 80% when over 20 units -> 30% when over 4 units, time 45 m -> 30 m (chimera 1 hour), added to adv_chemistry book

- mutagen: difficulty 6 -> 4

// Does not include Medical, Alpha and Elf-A.
- animal mutagens: difficulty 7 -> 5, removed themed requirements (now 1 sample and mutagen is enough)

- chimera/alpha/medical/elf-a mutagen: difficulty 8 -> 7, time varied -> 20 m

- chimera sample: removed mutagen requirement, added to adv_chemistry book

This allows basic mutagen to be accessible without breaking into a late game labs and without needing an extremely high applied science skill. For some reason fish and crustascean mutagen had higher requirements than the primer version.
For the more exotic types, obtaining them remains a challenge for those who still want the full journey to mutant from vanilla.

--Tools--  
- separation funnel: difficulty 7 -> 5

---Item groups (item spawns)---  
- science: added spectrophotometer, ph meter, melting point aparatus and separation funnel at 3% chance

- chem lab: added spectrophotometer, ph meter, melting point aparatus and separation funnel at 5% chance

These couple of changes will allow the tools required for mutagen production to spawn in more places with more regularity. Specially other non-mutagen related labs. (The rarest item, the spectrophotometer, can be bough on Amazon for 300 bucks, how is it THIS rare?)


## License
Do whatever.

